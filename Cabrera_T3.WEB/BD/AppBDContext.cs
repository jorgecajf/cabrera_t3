﻿using Cabrera_T3.WEB.BD.Configurations;
using Cabrera_T3.WEB.Models;
using Microsoft.EntityFrameworkCore;

namespace Cabrera_T3.WEB.BD
{
    public class AppBDContext : DbContext
    {

        public AppBDContext(DbContextOptions<AppBDContext> options) : base(options)
        { }

        public DbSet<HistoriaClinica> HistoriasClinicas { get; set; }
        public DbSet<User> Usuarios { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new HistoriaClinicaConfigurations());
            modelBuilder.ApplyConfiguration(new UserConfigurations());
        }
    }
}
