﻿using Cabrera_T3.WEB.BD;
using Cabrera_T3.WEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cabrera_T3.WEB.Repositories
{
    public interface IHistoriaRepository
    {
        public List<HistoriaClinica> ListaHistoriasClinicasBD();
        public List<string> ListaSexo();
        public List<string> ListaEspecie();
        public List<string> ListaRaza();
        public void AñadirHistorias(HistoriaClinica historiaClinica);
        public int ExisteCodigoRegistro(string codigo);
    }
    public class HistoriaRepository : IHistoriaRepository
    {
        private readonly AppBDContext context;

        public HistoriaRepository(AppBDContext context)
        {
            this.context = context;
        }

        public List<HistoriaClinica> ListaHistoriasClinicasBD()
        {
            return context.HistoriasClinicas.ToList();
        }

        public List<string> ListaRaza()
        {
            var razas = new List<string>();

            razas.Add("Akita");
            razas.Add("Beagle");
            razas.Add("Boxer");
            razas.Add("Alaskan Malamute");
            razas.Add("Chihuahua");
            razas.Add("Dálmata");
            razas.Add("Dobermann");
            razas.Add("Husky Siberiano");
            razas.Add("Golden Retriever");
            razas.Add("Dogo Argentino");

            return razas;
        }
        public List<string> ListaEspecie()
        {
            var especies = new List<string>();

            especies.Add("Perro");
            especies.Add("Gato");
            especies.Add("Ave");

            return especies;
        }
        public List<string> ListaSexo()
        {
            var sexos = new List<string>();

            sexos.Add("Macho");
            sexos.Add("Hembra");

            return sexos;
        }

        public void AñadirHistorias(HistoriaClinica historiaClinica)
        {
            context.HistoriasClinicas.Add(historiaClinica);
            context.SaveChanges();
        }

        public int ExisteCodigoRegistro(string codigo)
        {
            return context.HistoriasClinicas.Where(o => o.CodigoRegistro == codigo).Count();
            
        }
    }
}
