﻿using System;
using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc;
using Cabrera_T3.WEB.Repositories;
using Cabrera_T3.WEB.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace Cabrera_T3.WEB.Controllers
{
    public class UserController : Controller
    {
        private readonly IUsuarioRepository repository;
        private readonly ICookieAuthService cookieService;
        public UserController(IUsuarioRepository repository, ICookieAuthService cookieService)
        {
            this.repository = repository;
            this.cookieService = cookieService;
        }

        [HttpGet]
        public IActionResult Login()
        {
 
            return View();
        }

        [HttpPost]
        public IActionResult Login(string nombreUsuario, string password)
        {

            var usuariodb = repository.FindUserLogin(nombreUsuario, password);
            if (usuariodb == null)
            {
                ModelState.AddModelError("Usuario", "Usuario y/o contraseña incorrectos");
                return View();
            }
            cookieService.SetHttpContext(HttpContext);
            cookieService.SetSessionContext(usuariodb);
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name,usuariodb.Username)
            };

            var Identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var principal = new ClaimsPrincipal(Identity);

            cookieService.Login(principal);
            return RedirectToAction("VerHistorias", "HistoriaClinica");
        }
        [HttpGet]
        public IActionResult Logout()
        {
            cookieService.SetHttpContext(HttpContext);
            cookieService.LogOut();
            return RedirectToAction("Index", "HistoriaClinica");
        }
    }
}
