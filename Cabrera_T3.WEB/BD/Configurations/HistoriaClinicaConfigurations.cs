﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Cabrera_T3.WEB.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cabrera_T3.WEB.BD.Configurations
{
    public class HistoriaClinicaConfigurations : IEntityTypeConfiguration<HistoriaClinica>
    {
        public void Configure(EntityTypeBuilder<HistoriaClinica> builder)
        {
            builder.ToTable("HistoriaClinica");
            builder.HasKey(o => o.HistoriaClinicaId);

        }
    }
}
