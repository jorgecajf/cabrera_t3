﻿using System;
using System.Linq;
using Cabrera_T3.WEB.Models;
using Cabrera_T3.WEB.BD;
using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;

namespace Cabrera_T3.WEB.Repositories
{
    public interface IUsuarioRepository
    {
        public User FindUserLogin(string nombreUsuario, string password);
        public bool ComprobarFormatoEmail(string email);
    }
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly AppBDContext context;

        public UsuarioRepository(AppBDContext context)
        {
            this.context = context;
        }
        public User FindUserLogin(string nombreUsuario, string password)
        {
            return context.Usuarios.FirstOrDefault(o => o.Username == nombreUsuario && o.Password == password);
        }

        public bool ComprobarFormatoEmail(string email)
        {
            String sFormato;
            sFormato = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, sFormato))
            {
                if (Regex.Replace(email, sFormato, String.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

    }
   
}
