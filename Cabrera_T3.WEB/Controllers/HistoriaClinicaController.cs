﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Cabrera_T3.WEB.Repositories;
using Cabrera_T3.WEB.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Cabrera_T3.WEB.Models;

namespace Cabrera_T3.WEB.Controllers
{
    public class HistoriaClinicaController : Controller
    {
        private readonly IUsuarioRepository repositoryU;
        private readonly ICookieAuthService cookieService;
        private readonly IHistoriaRepository repositoryH;


        public HistoriaClinicaController(IUsuarioRepository repository, ICookieAuthService cookieService, IHistoriaRepository repositoryH)
        {
            this.repositoryU = repository;
            this.cookieService = cookieService;
            this.repositoryH = repositoryH;
        }
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public IActionResult VerHistorias()
        {
            var model = repositoryH.ListaHistoriasClinicasBD();

            return View(model);
        }

        [Authorize]
        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.Sexo = repositoryH.ListaSexo();
            ViewBag.Raza = repositoryH.ListaRaza();
            ViewBag.Especie = repositoryH.ListaEspecie();

            return View(new HistoriaClinica());
        }
        //[Authorize]
        [HttpPost]
        public IActionResult Create(HistoriaClinica historiaClinica)
        {

            var fechaActual = DateTime.Now;

            if (repositoryH.ExisteCodigoRegistro(historiaClinica.CodigoRegistro) > 0)
            {
                ModelState.AddModelError("CodigodeRegistro", "Ya existe ese código de registro");

            }

            historiaClinica.FechaRegistro = fechaActual;

            if (historiaClinica.NacimientoMascota > fechaActual)
            {
                ModelState.AddModelError("Fecha", "La fecha de nacimiendo no puede ser mayor a la fecha actual");
            }

            if (!repositoryU.ComprobarFormatoEmail(historiaClinica.Email))
            {
                ModelState.AddModelError("FormatoEmail", "El correo introducido no cumple con el formato Email");
            }

            if (ModelState.IsValid)
            {
                repositoryH.AñadirHistorias(historiaClinica);
                return RedirectToAction("VerHistorias", "HistoriaClinica");
            }

            ViewBag.Sexo = repositoryH.ListaSexo();
            ViewBag.Raza = repositoryH.ListaRaza();
            ViewBag.Especie = repositoryH.ListaEspecie();
            return View(historiaClinica);
        }

    }
}
