using Cabrera_T3.WEB.Controllers;
using Cabrera_T3.WEB.Models;
using Cabrera_T3.WEB.Repositories;
using Cabrera_T3.WEB.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;

namespace Cabrera_T3.TEST
{
    public class UserControllerTest
    {

        [Test]
        public void LoginCorrecto()
        {
            var mockRepository = new Mock<IUsuarioRepository>();
            mockRepository.Setup(o => o.FindUserLogin(It.IsAny<String>(), It.IsAny<String>()))
                .Returns(new User { Username = "admin", Password = "admin" });

            var cookie = new Mock<ICookieAuthService>();

            var controller = new UserController(mockRepository.Object, cookie.Object);

            var view = controller.Login("admin", "admin");

            Assert.IsInstanceOf<RedirectToActionResult>(view);
        }

        [Test]
        public void LoginInCorrecto()
        {
            var mockRepository = new Mock<IUsuarioRepository>();
            mockRepository.Setup(o => o.FindUserLogin("admin", It.IsAny<String>()));

            var cookie = new Mock<ICookieAuthService>();

            var controller = new UserController(mockRepository.Object, cookie.Object);

            var view = controller.Login("admin", "hola");

            Assert.IsInstanceOf<ViewResult>(view);
        }
    }
}