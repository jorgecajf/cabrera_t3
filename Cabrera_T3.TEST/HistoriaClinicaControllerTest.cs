using Cabrera_T3.WEB.Controllers;
using Cabrera_T3.WEB.Models;
using Cabrera_T3.WEB.Repositories;
using Cabrera_T3.WEB.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Cabrera_T3.TEST
{
    public class HistoriaClinicaControllerTest
    {

        [Test]
        public void VerHistoriasRetornaVistaDeHistorias()
        {
            var repositoryU = new Mock<IUsuarioRepository>();
            var repositoryH = new Mock<IHistoriaRepository>();

            var cookie = new Mock<ICookieAuthService>();

            var controller = new HistoriaClinicaController(repositoryU.Object, cookie.Object, repositoryH.Object);

            var view = controller.VerHistorias();

            Assert.IsInstanceOf<ViewResult>(view);
        }

        [Test]
        public void CrearHistoriaCorrectamente()
        {
            var repositoryU = new Mock<IUsuarioRepository>();
            repositoryU.Setup(o => o.ComprobarFormatoEmail(It.IsAny<string>())).Returns(true);
            var repositoryH = new Mock<IHistoriaRepository>();
            repositoryH.Setup(o => o.ExisteCodigoRegistro(It.IsAny<string>())).Returns(0);
            var cookie = new Mock<ICookieAuthService>();

            var controller = new HistoriaClinicaController(repositoryU.Object, cookie.Object, repositoryH.Object);

            var view = controller.Create(new HistoriaClinica());

            Assert.IsInstanceOf<RedirectToActionResult>(view);
        }

        [Test]
        public void CrearHistoriaInCorrectamenteCuandoYaExisteCodigodeRegistro()
        {
            var repositoryU = new Mock<IUsuarioRepository>();
            repositoryU.Setup(o => o.ComprobarFormatoEmail(It.IsAny<string>())).Returns(true);
            var repositoryH = new Mock<IHistoriaRepository>();
            repositoryH.Setup(o => o.ExisteCodigoRegistro(It.IsAny<string>())).Returns(1);
            repositoryH.Setup(o => o.ListaEspecie()).Returns(It.IsAny<List<string>>());
            repositoryH.Setup(o => o.ListaRaza()).Returns(It.IsAny<List<string>>());
            repositoryH.Setup(o => o.ListaSexo()).Returns(It.IsAny<List<string>>());
            var cookie = new Mock<ICookieAuthService>();

            var controller = new HistoriaClinicaController(repositoryU.Object, cookie.Object, repositoryH.Object);

            var view = controller.Create(new HistoriaClinica());

            Assert.IsInstanceOf<ViewResult>(view);
        }

        [Test]
        public void CrearHistoriaInCorrectamenteCuandoElEmailNoCumpleElFormato()
        {
            var repositoryU = new Mock<IUsuarioRepository>();
            repositoryU.Setup(o => o.ComprobarFormatoEmail(It.IsAny<string>())).Returns(false);
            var repositoryH = new Mock<IHistoriaRepository>();
            repositoryH.Setup(o => o.ExisteCodigoRegistro(It.IsAny<string>())).Returns(0);
            repositoryH.Setup(o => o.ListaEspecie()).Returns(It.IsAny<List<string>>());
            repositoryH.Setup(o => o.ListaRaza()).Returns(It.IsAny<List<string>>());
            repositoryH.Setup(o => o.ListaSexo()).Returns(It.IsAny<List<string>>());
            var cookie = new Mock<ICookieAuthService>();

            var controller = new HistoriaClinicaController(repositoryU.Object, cookie.Object, repositoryH.Object);

            var view = controller.Create(new HistoriaClinica());

            Assert.IsInstanceOf<ViewResult>(view);
        }
        [Test]
        public void CrearHistoriaInCorrectamenteCuandoLaFechadeNacimientoDeLaMascotaEsMayorALaFechaActual()
        {
            var repositoryU = new Mock<IUsuarioRepository>();
            repositoryU.Setup(o => o.ComprobarFormatoEmail(It.IsAny<string>())).Returns(true);
            var repositoryH = new Mock<IHistoriaRepository>();
            repositoryH.Setup(o => o.ExisteCodigoRegistro(It.IsAny<string>())).Returns(0);
            repositoryH.Setup(o => o.ListaEspecie()).Returns(It.IsAny<List<string>>());
            repositoryH.Setup(o => o.ListaRaza()).Returns(It.IsAny<List<string>>());
            repositoryH.Setup(o => o.ListaSexo()).Returns(It.IsAny<List<string>>());
            var cookie = new Mock<ICookieAuthService>();

            var controller = new HistoriaClinicaController(repositoryU.Object, cookie.Object, repositoryH.Object);

            var view = controller.Create(new HistoriaClinica() { NacimientoMascota = new DateTime(2020, 12, 1, 8, 30, 52)});

            Assert.IsInstanceOf<ViewResult>(view);
        }
    }
}