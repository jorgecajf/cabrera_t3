﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cabrera_T3.WEB.Models
{
    public class HistoriaClinica
    {
        public int HistoriaClinicaId { get; set; }
        [Required]
        public string CodigoRegistro { get; set; }
        [Required]
        public DateTime FechaRegistro { get; set; }
        [Required]
        public string NombreMascota { get; set; }
        [Required]
        public DateTime NacimientoMascota { get; set; }
        public string Sexo { get; set; }
        public string Especie { get; set; }
        public string Raza { get; set; }
        public string Tamanio { get; set; }
        public string DatosParticulares { get; set; }
        [Required]
        public string NombresDuenio { get; set; }
        [Required]
        public string DireccionDuenio { get; set; }
        [Required]
        public string Telefono { get; set; }
        [EmailAddress]
        public string Email { get; set; }
    }
}
