﻿using Cabrera_T3.WEB.Extensions;
using Cabrera_T3.WEB.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Cabrera_T3.WEB.Services
{
    public interface ICookieAuthService
    {
        public void SetHttpContext(HttpContext httpContext);
        public void SetSessionContext(User usuariodb);
        public void Login(ClaimsPrincipal claim);
        public User UsuarioLogueado();
        public void LogOut();
    }
    public class CookieAuthService : ICookieAuthService
    {
        private HttpContext httpContext;
        public void SetHttpContext(HttpContext httpContext)
        {
            this.httpContext = httpContext;
        }
        public void Login(ClaimsPrincipal claim)
        {
            httpContext.SignInAsync(claim);
        }
        public void SetSessionContext(User usuariodb)
        {
            httpContext.Session.Set("LoggedUser", usuariodb);
        }
        public User UsuarioLogueado()
        {
            return httpContext.Session.Get<User>("LoggedUser");
        }
        public void LogOut()
        {
            httpContext.SignOutAsync();
        }
    }
}
